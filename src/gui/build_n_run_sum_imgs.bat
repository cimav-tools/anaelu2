echo removing previous binrs, objs and imgs
del /f anaelu_img_add img_sum.edf
del /f anaelu_img_add.exe
del /f *.o *.mod

echo compiling
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall tools.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall anaelu_img_add.f90

echo linking
gfortran -o anaelu_img_add.exe *.o -static

echo running binary exe
rem anaelu_img_add.exe img_64bit_xrd.edf img_smooth.edf img_sum.edf

del /f *.o *.mod

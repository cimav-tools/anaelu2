echo "removing previous binrs, objs and imgs"
rm anaelu_img_smooth img_smooth.edf
rm anaeli_img_smooth.exe
rm *.o *.mod

echo "compiling"
gfortran -c -Wall input_args.f90
gfortran -c -Wall rw_n_use_edf.f90
gfortran -c -Wall tools.f90

gfortran -c -Wall anaelu_img_smooth.f90

echo "linking"
gfortran -o anaelu_img_smooth.exe *.o -static

echo "running binary exe"
./anaelu_img_smooth.exe cuted.edf img_smooth.edf 5

rm *.o *.mod

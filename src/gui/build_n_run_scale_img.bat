echo removing previous binrs, objs and imgs
del /f anaelu_img_scale scaled_img.edf
del /f anaelu_img_scale.exe
del /f *.o *.mod

echo compiling
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall tools.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall anaelu_img_scale.f90

echo linking
gfortran -o anaelu_img_scale.exe *.o -static

echo running binary exe
rem anaelu_img_scale.exe img_64bit_xrd.edf scaled_img.edf 0.55

del /f *.o *.mod

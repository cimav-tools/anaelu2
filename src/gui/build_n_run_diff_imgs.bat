echo "removing previous binrs, objs and imgs"

del /f anaelu_dif_res.exe info_dif.dat img_diff.edf
del /f *.o *.mod

echo compiling
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall input_args.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall rw_n_use_edf.f90
gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall tools.f90

gfortran -c -O2 -ffree-line-length-none -funroll-loops -Wall anaelu_dif_res.f90

echo linking
gfortran -o anaelu_dif_res.exe *.o -static

echo "running binary exe"
rem anaelu_dif_res img_xrd_tot.edf img_smooth.edf img_diff.edf info_dif.dat

del /f *.o *.mod

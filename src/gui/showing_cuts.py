import sys
import os
import numpy as np
import time

from PySide2 import QtUiTools
from PySide2.QtCore import *
from PySide2.QtWidgets import *
from PySide2.QtGui import *

from img_rgb_gen import np2bmp_heat

class CutsImgagSene(QGraphicsScene):
    new_list = Signal(list)
    def __init__(self, parent = None):
        super(CutsImgagSene, self).__init__(parent)
        self.parent = parent
        print("__init__(CutsImgagSene)")

    def re_draw_me(
        self, trim_cut_exp, trim_cut_cal, trim_cut_bak, trim_cut_dif
    ):
        self.clear()

        i_max = max(np.amax(trim_cut_exp), np.amax(trim_cut_cal))
        y_scale = -500.0 / i_max
        #x_scale = 100.0 / i_max

        x_old_exp, y_old_exp = 0, 0
        x_old_cal, y_old_cal = 0, 0
        x_old_bak, y_old_bak = 0, 0
        x_old_dif, y_old_dif = 0, 0

        exp_pen = QPen(
            Qt.blue, 1.6, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )
        cal_pen = QPen(
            Qt.red, 1.6, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )

        bak_pen = QPen(
            Qt.green, 1.6, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )


        dif_pen = QPen(
            Qt.magenta, 1.6, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )

        axs_pen = QPen(
            Qt.black, 1.2, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )

        '''dif_pen = QPen(
            Qt.cyan, 1.6, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin
        )'''

        step_div_size = 110
        self.addLine(-step_div_size, 0, len(trim_cut_exp) + step_div_size, 0, axs_pen)

        for x_ini in range(len(trim_cut_exp)):
            x = float(x_ini)
            y_exp = float(trim_cut_exp[x_ini])
            y_cal = float(trim_cut_cal[x_ini])
            y_bak = float(trim_cut_bak[x_ini])
            y_dif = float(trim_cut_dif[x_ini])

            self.addLine(
                x_old_exp, y_old_exp* y_scale,
                x, y_exp* y_scale, cal_pen
            )
            x_old_exp, y_old_exp = x, y_exp

            self.addLine(
                x_old_exp, y_old_cal * y_scale,
                x, y_cal * y_scale, exp_pen
            )
            x_old_cal, y_old_cal = x, y_cal

            self.addLine(
                x_old_bak, y_old_bak * y_scale,
                x, y_bak * y_scale, bak_pen
            )
            x_old_bak, y_old_bak = x, y_bak

            self.addLine(
                x_old_dif, y_old_dif * y_scale + step_div_size,
                x, y_dif * y_scale + step_div_size, dif_pen
            )

            x_old_dif, y_old_dif = x, y_dif

    def mousePressEvent(self, event):
        ev_pos = event.scenePos()
        print("mousePressEvent")
        x, y = ev_pos.x(), ev_pos.y()
        print("x, y =", x, y)
        self.x_ini, self.y_ini = x, y
        self.pressed = True

    def mouseReleaseEvent(self, event):
        print("mouseReleaseEvent")
        self.pressed = False

    def mouseMoveEvent(self, event):
        if self.pressed == True:
            ev_pos = event.scenePos()
            self.rc_x2, self.rc_y2 = ev_pos.x(), ev_pos.y()
            self.rc_x1 = self.x_ini
            self.rc_y1 = self.y_ini
            #self.re_draw_me()


class show_cut(QObject):
    leaving = Signal()
    def __init__(self, parent = None):
        super(show_cut, self).__init__(parent)

        my_code_path = os.path.dirname(os.path.abspath(__file__))
        ui_path = my_code_path + os.sep + "cuts.ui"
        print("ui_path = ", ui_path)
        self.dialog = QtUiTools.QUiLoader().load(ui_path)
        self.dialog.setWindowTitle("Image Cuts")
        self.dialog.show()
        self.dialog.SaveButton.clicked.connect(self.on_save_click)

        self.dialog.finished.connect(self.close1Event)

        self.my_scene = CutsImgagSene(self)
        self.dialog.CurGraphicsView.setScene(self.my_scene)
        self.dialog.CurGraphicsView.setDragMode(QGraphicsView.NoDrag)


        #print(dir(self.dialog))

    def on_save_click(self):
        print("Save clicked")

    def repaint(self, array1, array2, array3, array4):
        self.my_scene.re_draw_me(array1, array2, array3, array4)

    def close1Event(self):
        print("close1Event")
        self.leaving.emit()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    form = show_cut(None)
    sys.exit(app.exec_())

